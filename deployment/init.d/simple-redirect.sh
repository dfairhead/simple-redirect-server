#!/bin/sh
### BEGIN INIT INFO
# Provides:          simple-redirect
# Required-Start:    $network
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Simple Redirect script
# Description:       Simple localhost web server for redirecting to somewhere.
### END INIT INFO
#
# Author:	Daniel Fairhead
#
set -e


case "$1" in
  start)
    echo "Loading SIMPLE-SERVER"
    nohup su -c /usr/local/bin/simple-redirect.py nobody &> /dev/null
    ;;
  stop)
    echo "Killing all SIMPLE-SERVER stuff"
    kill "$(ps -u nobody -o pid,command |grep python\.\*simple-redirect|awk '{print $1}')"
    ;;
  *)
    echo "Usage: /etc/init.d/simple-server {start|stop}"
    exit 1
    ;;
esac

exit 0
