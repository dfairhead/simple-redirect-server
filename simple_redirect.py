#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' simple_redirect.py - (C) 2013-2017 Daniel Fairhead

    ------------------
    A simple localhost-only server which if it can, redirects
    to another site, but if that site is unavailable, returns a sensible
    error message, and retries every so often.
    ------------------

    simple_redirect is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    simple_redirect is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with simple_redirect.  If not, see <http://www.gnu.org/licenses/>.


'''


import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import socket

import BaseHTTPServer
from urllib2 import urlopen
from string import Template
from os.path import exists
from ConfigParser import RawConfigParser
from collections import defaultdict

CONF_FILE = '/etc/simple_redirect.conf'

_defaults = {
    'server': {
        'redirect_to_url': 'https://streetsign.org.uk/',
        'timeout': 10,
        'listen_ip': '127.0.0.1',
        'listen_port': 8080,
        'retry_time': 5000,
        },
    'page': {
        'background': '#000',
        'title_color': '#FFF',
        'msg_color': '#AAA',
        'title': 'Simple Redirect',
        'title_size': '50pt',
        'msg_size': '30pt',
        'font': 'Sans Serif',
        'sorry_msg': 'Sorry, I cannot reach %URL%.<br/>'
                     ' I\'ll try again soon.<br/>'
                     ' My IP is: <i>%IPS%</i>',
    }}


def load_config():
    """
        Read the config file, and return a 'getter' which gives either a value
        from the config, or one of the defaults.
    """
    try:
        conf = RawConfigParser()
        conf.read(CONF_FILE)

        def get(section, value):
            try:
                return conf.get(section, value)
            except:
                return _defaults[section][value]

    except Exception as e:
        print e
        print('Cannot read config file:' + CONF_FILE)
        def get(section, value):
            return _defaults[section][value]

    return get


TEMPLATE=Template("""<!doctype html>
<html>
<head>
    <title>Simple Redirector</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style>
    body {
        background: $background;
        color: $msg_color;
        font-family: $font;
        text-align: center;
        padding: 10%;
        overflow: hidden;
    }
    h1 {
        color: $title_color;
        font-size: $title_size;
    }
    p {
        color: $msg_color;
        font-size: $msg_size;
    }
    </style>
</head>
<body>
    <h1>$title</h1>
    <p>$msg</p>
    <script type="text/javascript">
    setInterval(function () { location.reload(); }, $retry_time);
    </script>
</body>
</html>
""")


def get_my_ips():
    ''' returns a list of this computer's current IP addresses '''
    return (list(set(([ip for ip in
        socket.gethostbyname_ex(socket.gethostname())[2]
        if not ip.startswith("127.")][:1]))))

def can_i_reach(url, timeout):
    ''' try and load the config file (in case it's changed), and then try
        to open a connection to the specifiec server, and return the status,
        and a message (either the URL, if successful, or an error message. '''

    try:
        urlopen(url, data=None, timeout=timeout).read()
        return True
    except:
        pass

    return False


class RedirectHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    config = None
    online = False

    def redirect(self):
        ''' send redirect headers for the specified URL '''

        self.send_response(307)
        self.send_header("Location", self.config('server', 'redirect_to_url'))
        self.end_headers()

    def html_headers(self):
        ''' send the headers for the 'Sorry no can do' page. '''
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def init(self):
        self.config = load_config()

        self.online = can_i_reach(
            self.config('server','redirect_to_url'),
            int(self.config('server', 'timeout')))

    def do_HEAD(self):
        self.init()

        if self.online:
            print "Redirecting to ", self.config('server', 'redirect_to_url')
            self.redirect()
        else:
            self.html_headers()

    def do_GET(self):
        self.init()
        self.do_HEAD()

        if not self.online:
            self.wfile.write(TEMPLATE.substitute(
                msg=self.config('page', 'sorry_msg')
                    .replace('%URL%', self.config('server','redirect_to_url'))
                    .replace('%IPS%', ', '.join(get_my_ips())),
                title=self.config('page', 'title'),
                background=self.config('page', 'background'),
                font=self.config('page', 'font'),
                title_color=self.config('page', 'title_color'),
                msg_color=self.config('page', 'msg_color'),
                title_size=self.config('page', 'title_size'),
                msg_size=self.config('page', 'msg_size'),
                retry_time=max(int(self.config('server', 'retry_time')), 1000),
                ))


if __name__ == '__main__':
    if len(sys.argv) == 2:
        if exists(sys.argv[1]):
            CONF_FILE = sys.argv[1]

    config = load_config()

    server_class = BaseHTTPServer.HTTPServer
    server = server_class(
        (config('server', 'listen_ip'), int(config('server', 'listen_port'))),
        RedirectHandler)
    print 'Listening on: (%s port %s)' % (config('server', 'listen_ip'),
                                          config('server', 'listen_port'))
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()

