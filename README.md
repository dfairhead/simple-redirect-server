# Simple Redirect

Runs a very simple localhost python based web server, which trys to connect
to a URL, specified in /etc/simple_redirect.conf

If it can connect, then it redirects the client to the specified URL.

If it cannot, then it displays a subtle message about it, including the local
machine IPs, to make life easier for the sysadmin, and retries every 5 seconds
until it can connect.

## Options:

(found in `/etc/simple_redirect.conf`) Normal INI file style. ` ;` for comments.

```
    [server]
    redirect_to_url=https://duckduckgo.com ; Where do you want to go today?
    timeout=10 ; how long should I try for? (seconds)
    listen_ip=127.0.0.1 ; what interfaces should this server listen on?
    listen_port=8080 ; what port should I listen on?
    retry_time=5000 ; how often should I retry? (milliseconds)

    [page]
    background=#000 ; a CSS background style.
    title_color=rgb(255,255,255) ; CSS color for title
    msg_color=#aaa ; CSS color for title
    title_size=50pt
    msg_size=30pt
    title=Simple Redirect
    sorry_msg=Sorry, server is offline...
    font=Comic Sans MS ; because why not?

```

The `sorry_msg` value can have the following 'magic variables'

* `%URL%` -> becomes the redirect_to_url
* `%IPS%` -> becomes the list of IP addresses of this computer (useful for debugging)

The config file is read on every request, so changes to the file are reflected instantly, except for the server listen_ip and listen_port settings.  If you need to change those, you'll need to restart it.

## Deployment:

Usually copy the simple_redirect.py to /usr/local/bin/, chmod +x it,
Set up your etc/simple_redirect.conf however you wish.  Either then run it
directly, or else use the operating system.

There's sysv and systemd template services in `deployment`.

For systemd, copy `deployment/systemd/simple_redirect.service` to
`/etc/systemd/system/simple_redirect.service`, and then run
`systemctl enable simple_redirect.service`

You can also call the simple_redirect.py script with the name of
the config file you want to use.

## Requirements:

Only a reasonably modern python (>=2.6ish or so should work)
